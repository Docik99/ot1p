import pytest
from LR3 import *

# HOST = 'localhost'
# PORT = 9200
# #INDEX_NAME = '2018-3-09-doc-lr3-test1'


def test_create():
    start = Start()
    start.create()
    assert start.elastic.indices.exists(index=INDEX_NAME) is True


def test_add_fb2():
    start = Start()
    start.create()
    author = 'Толстой'
    name = 'Война и мир'
    file_path = 'Война и мир - Толстой - том 1.fb2'

    assert start.add_fb2(author, name, file_path) == 69
    start.elastic.indices.refresh(index=INDEX_NAME)


def test_add_fb2_duplicate_chapter():
    author = 'Толстой'
    name = 'Война и мир'
    file_path = 'Война и мир - Толстой - том 1.fb2'
    start = Start()
    start.create()
    assert start.add_fb2(author, name, file_path) == 0
    start.elastic.indices.refresh(index=INDEX_NAME)


def test_parcer():
    with pytest.raises(IsADirectoryError):
        parcer("input")
    with pytest.raises(FileNotFoundError):
        parcer("input/books/1.txt")

def test_get_chapter_correct_date():
    start = Start()
    start.create()
    start.add_fb2('Толстой', 'Война и мир', 'Война и мир - Толстой - том 1.fb2')
    text = start.get_chapter('1-1-1')
    assert len(text) == 10744
    with pytest.raises(ValueError):
        start.get_chapter('1-1-1', "qwerty")
    text = start.get_chapter('1-2-3', 732)
    assert len(text) == 732
    text = start.get_chapter('1 - 2 - 3')
    assert text == 0
    text = start.get_chapter('SEVEN-3-2')
    assert text == 0

def test_get_text():
    start = Start()
    start.create()
    start.add_fb2('Толстой', 'Война и мир', 'Война и мир - Толстой - том 2.fb2')
    start.elastic.indices.refresh(index=INDEX_NAME)
    text = "Пьер уехал домой,  а  Ростов  с  Долоховым  и Денисовым до позднего вечера"
    assert start.get_text(text).hits.total == 1
    assert start.get_text(text).hits[0].meta.id == '2-1-4'


def test_summarize_text_with_correct_text():
    start = Start()
    start.create()
    start.add_fb2('Толстой', 'Война и мир', 'Война и мир - Толстой - том 2.fb2')
    assert len(start.summarize_text('Пьер  уехал  домой,  а  Ростов  с  Долоховым  и Денисовым до позднего вечера')) == 2775
    assert len(start.summarize_text(None, '2-1-4')) == 2775

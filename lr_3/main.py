"""
Скрипт для работы с elasticsearch и анализа большого произведения

usage: main.py [-h] [-p PORT] [-s HOST] [-a AUTHOR] [-n NAME]
               [-l LIMIT] [-t TEXT] [-c CHAPTER] command [second_command]

1) Создание индекса (create)
2) Загрузка заданного файла формата fb2 (add-fb2)
3) Вывод заданной главы текста с опциональным ограничением числа выводимых символов (get-chapter)
4) Поиск по переданной фразе (get-text)
5) Реферирование указанной главы или первой главы с указанной фразой (summarize-text)
"""

import argparse
import re
import sys
from heapq import nlargest
from string import punctuation

from bs4 import BeautifulSoup
from elasticsearch_dsl import Document, connections, analysis, Text
from elasticsearch_dsl import analyzer, tokenizer, Search
import spacy
from spacy.lang.ru.stop_words import STOP_WORDS
from IPython.utils import io
from rom import rom_parse

HOST = 'localhost'
PORT = 9200
INDEX_NAME = '12018-3-09-doc-lr3'


def arg_parse():
    """Обработка аргументов командной строки

    Возвращаемые значения:
        argument: введенные аргументы
    """
    argument = argparse.ArgumentParser()
    argument.add_argument("command")
    argument.add_argument("second_command", nargs='?', default=None)
    argument.add_argument("-p", "--port", type=int, default=9200)
    argument.add_argument("-s", "--host", type=str, default='localhost')
    argument.add_argument("-a", "--author")
    argument.add_argument("-n", "--name")
    argument.add_argument("-l", "--limit")
    argument.add_argument("-t", "--text")
    argument.add_argument("-c", "--chapter")

    return argument.parse_args()


def parcer(path):
    """Парсер книги в формате fb2 для получения разметки книги
    Аргументы:
            fullPath: путь к файлу книги

    Возвращаемые значения:
        responce: разметка книги

    """
    responce = []
    try:
        with open(path, "r", encoding='UTF-8') as f_in:
            soup = BeautifulSoup(f_in, 'html.parser')
            tags = soup.find_all('section')
            volume = 0
            part = 0
            for tag in tags:
                if tag.title is None:
                    continue

                elif re.search(r'.* Том .*', tag.title.p.get_text()):
                    volume = int(tag.title.p.get_text().split()[-1])

                elif re.search(r'\* ЭПИЛОГ \*', tag.title.p.get_text()):
                    volume = "Эпилог"
                    part = 0

                elif re.search(r'\*.*ЧАСТЬ.*\*', tag.title.p.get_text()):
                    part += 1

                else:
                    content = ""
                    arr_tegs = tag.find_all('p')
                    for item in arr_tegs:
                        content += re.sub(r'\s+', ' ', item.get_text()) + ' '
                    chapter = content.split(" ")[0]
                    chapter = chapter.replace(".", "")
                    chapter = chapter.replace("Х", "X")
                    if chapter != "Примечания":
                        chapter = rom_parse(chapter)
                    adder = {"id": f"{volume}-{part}-{chapter}", "content": content}
                    responce.append(adder)
        return responce
    except FileNotFoundError as err:
        print(f"Error {err}")
        raise

    except IsADirectoryError as err:
        print(f"Error {err}")
        raise


'''стек слов, неиндексируемых эластиком'''
russian_stop = analysis.token_filter('rus_stop', type='stop', stopwords='_russian_')

'''анализирует файл по 2 токинизаторам стоп слов'''
analyzer = analyzer('analyzer', tokenizer=tokenizer('standard'),
                    filter=['lowercase', russian_stop])


class Parametrs(Document):
    """Парамметры индекса"""
    '''indices'''
    title = Text(analyzer=analyzer)
    author = Text(analyzer=analyzer)
    content = Text(analyzer=analyzer)

    class Index:
        """Настройки индекса"""
        name = INDEX_NAME
        settings = {
            "number_of_shards": 5,
            "number_of_replicas": 5,
        }


class Start:
    """Основной класс работы с elasticsearch"""

    def __init__(self):
        self.elastic = connections.create_connection(host=HOST, port=PORT)

    def create(self):
        """Создание индекса

        Аргументы:

        Возвращаемые значения:
            created: был ли создан новый индекс (True/False)

        """
        created = False
        body_books = {
            "settings": {
                "analysis": {
                    "filter": {
                        "russian_stop": {
                            "type": "stop",
                            "stopwords": "_russian_"
                        },
                        "russian_keywords": {
                            "type": "stop"
                        }
                    },

                    "analyzer": {
                        "custom_analyzer": {
                            "type": "custom",
                            "tokenizer": "standard",
                            "filter": [
                                "lowercase",
                                "russian_stop",
                                "russian_keywords"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "_doc": {
                    "properties": {
                        "title": {
                            "type": "text",
                            "analyzer": "standard",
                            "search_analyzer": "standard"
                        },
                        "author": {
                            "type": "text",
                            "analyzer": "standard",
                            "search_analyzer": "standard"
                        },
                        "content": {
                            "type": "text",
                            "analyzer": "custom_analyzer",
                            "search_analyzer": "custom_analyzer"
                        }
                    }
                }
            }
        }

        try:
            if not self.elastic.indices.exists(INDEX_NAME):
                self.elastic.indices.create(index=INDEX_NAME, ignore=400, body=body_books)
                print(f"Индекс: '{INDEX_NAME}' успешно создан!")
                created = True
            else:
                print(f"Индекс: '{INDEX_NAME}' уже существует!")
        except TypeError as ex:
            print(str(ex))

        return created

    def add_fb2(self, author, name, path):
        """Добавление книг формата fb2

        Аргументы:
            name: название книги
            author: автор
            path: путь к файлу книги

        Возвращаемые значения:
            count: количество созданых разделов

        """
        count = 0
        responce = parcer(f"{path}")
        for item in responce:
            if Parametrs.exists(id=item["id"]):
                print(f"Глава {item['id']} уже существует")
                continue
            parametrs = Parametrs()
            parametrs.title = str(name)
            parametrs.author = str(author)
            parametrs.content = str(item["content"])
            parametrs.meta.id = str(item["id"])
            parametrs.meta.doc = str("document")
            parametrs.save()
            print(f"Добавлена глава {item['id']}")
            count += 1
        self.elastic.indices.refresh(index=INDEX_NAME)
        return count

    def get_chapter(self, chapter, limit=None):
        """Вывод заданной главы текста в формате том-раздел-глава

        Аргументы:
            chapter: том-раздел-глава
            limit: максимальное количество символов

        Возвращаемые значения:
            content: текст заданной главы

        """
        if limit is not None:
            try:
                limit = int(limit)
            except ValueError as err:
                print(f"Error {err}")
                raise
        chapter = str(chapter).strip()
        if not re.search(r'^\d*\-\d*\-\d*$', chapter):
            return 0
        parametrs = Parametrs()
        content = parametrs.get(id=chapter).content[:limit]
        print(content)
        return content

    def get_text(self, text):
        """Поиск по переданной фразе

        Аргументы:
            text: фраза

        Возвращаемые значения:
            response: том-раздел-глава
        """
        search = Search(using=self.elastic, index=INDEX_NAME).query('match_phrase',
                                                                    **{'content': text})
        response = search.execute()
        for hit in response:
            print(hit.meta.id)
        return response

    def summarize_text(self, text=None, chapter=None):
        """Реферирование

        Аргументы:
            text: фраза
            chapter: том-раздел-глава

        Возвращаемые значения:
            summary: реферированный текст
        """
        if text is not None:
            if chapter is None:
                with io.capture_output():
                    content = self.get_text(text)[0].content
            else:
                if text is None and chapter is not None:
                    with io.capture_output():
                        content = self.get_chapter(chapter)
        else:
            if text is None and chapter is not None:
                with io.capture_output():
                    content = self.get_chapter(chapter)

        stopwords = list(STOP_WORDS)
        punctuations = punctuation + '\n'
        nlp = spacy.load('ru_core_news_sm')
        doc = nlp(content)
        word_frequencies = {}
        for word in doc:
            if word.text.lower() in stopwords or word.text.lower() in punctuations:
                continue
            if word.text not in word_frequencies:
                word_frequencies[word.text] = 1
            else:
                word_frequencies[word.text] += 1
        max_frequency = max(word_frequencies.values())
        for word in word_frequencies:
            word_frequencies[word] = word_frequencies[word] / max_frequency
        sentence_tokens = list(doc.sents)
        sentence_scores = {}
        for sent in sentence_tokens:
            for word in sent:
                if word.text.lower() in word_frequencies:
                    if sent not in sentence_scores:
                        sentence_scores[sent] = word_frequencies[word.text.lower()]
                    else:
                        sentence_scores[sent] += word_frequencies[word.text.lower()]
        select_length = int(len(sentence_tokens) * 0.3)
        summary = nlargest(select_length, sentence_scores, key=sentence_scores.get)
        final_summary = [word.text for word in summary]
        summary = ''.join(final_summary)
        print(summary)
        print(f"Длина исходного текста: {len(content)}")
        print(f"Длина реферированного текста: {len(summary)}")
        return summary


start = Start()


def main():
    """Передача аргументов командной строки исполняемым функциям"""
    args = arg_parse()

    if args.command == 'create':
        start.create()
        sys.exit(0)
    elif args.command == 'add-fb2':
        if args.name and args.author and args.second_command:
            start.add_fb2(args.author, args.name, args.second_command)
        else:
            print("Error args")
            sys.exit(1)
    elif args.command == 'get-chapter':
        if args.second_command and args.limit:
            start.get_chapter(args.second_command, args.limit)
        else:
            print("Error args")
            sys.exit(1)
    elif args.command == 'get-text':
        if args.second_command:
            start.get_text(args.second_command)
        else:
            print("Error args")
            sys.exit(1)
    elif args.command == 'summarize-text':
        if args.text or args.chapter:
            start.summarize_text(args.text, args.chapter)
        else:
            print("Error args")
            sys.exit(1)
    else:
        print("Unknown command")
        sys.exit(1)


if __name__ == '__main__':
    main()

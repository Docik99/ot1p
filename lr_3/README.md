# Лабораторная работа №3
Необходимо открыть терминал по адресу папки с ЛР №3 ../ot1p/lr_3

## Запуск Elasticsearch
В случае если контейнер, созданый в ЛР №2 (doc-lr-es) существует и запущен - ничего делать не нужно

Иначе, если контейнер существует, но не запущен

`$ docker start doc-lr-es`

Если же контейнер отсутствует

`$ docker-compose up -d`

## Создание образа

`$ docker build -t 2018-3-09-doc-lr3 .`

## Запуск образа

#### 0) Аргументы
`usage: main.py [-h] [-p PORT] [-s HOST] [-a AUTHOR] [-n NAME] [-l LIMIT] [-t TEXT] [-c CHAPTER] command [second_command]`

#### 1) Создание индекса
`$ docker run --rm --network host 2018-3-09-doc-lr3 create`

#### 2) Загрузка заданного файла формата fb2
`$ docker run --rm --network host -v `pwd`:/lr_3/ 2018-3-09-doc-lr3 add-fb2 "books/Война и мир - Толстой - том 1.fb2" --a Толстой --n "Война и Мир 1 том"`

#### 3) Вывод заданной главы текста в формате том-раздел-глава с опциональным ограничением числа выводимых символов
`$ docker run --rm --network host 2018-3-09-doc-lr3 get-chapter 1-2-3 --limit 70`

#### 4) Поиск по переданной фразе
`$ docker run --rm --network host 2018-3-09-doc-lr3 get-text "сопутствуемый австрийским"`

#### 5) Реферирование указанной главы или первой главы с указанной фразой
`$ docker run --rm --network host 2018-3-09-doc-lr3 summarize-text --text "сопутствуемый австрийским"`

`$ docker run --rm --network host 2018-3-09-doc-lr3 summarize-text --chapter 1-2-3`

## Запуск тестов
Предварительно необходимо в файле main.py изменить значение переменной "INDEX_NAME"

`$ pytest --cov=main tests.py -v`
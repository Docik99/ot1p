# Лабораторная работа №1

### Установка сервисов
Добавление репозитория Docker в репозиторий APT:

`$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"`

Обновление базы данных пакетов:

`$ sudo apt update`

Установление Docker:

`$ sudo apt install docker-ce`

### Ход работы
Запуск конфигурационного файла:

`$ sudo docker-compose up -d`

Проверка работы контейнеров:

`$ sudo docker ps`


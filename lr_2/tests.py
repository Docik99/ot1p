import pytest
from elasticsearch6 import Elasticsearch
from main import *

elastic = Elasticsearch([{'host': 'localhost', 'port': 9200}])


def test_connect():
    elastic = connect_elasticsearch('localhost', 9200)
    assert elastic.ping() is True
    elastic_false = connect_elasticsearch('falsehost', 9200)
    assert elastic_false.ping() is False


def test_create():
    create_index(elastic)
    assert elastic.indices.exists(INDEX_NAME) is True
    assert elastic.indices.exists('pytest') is False


def test_add_book1():
    assert add_book('voyna-i-mir.txt', elastic, 'Война И Мир', 'Толстой', '1865') is True


def test_add_book2():
    assert add_book('voyna-i-mir.txt', elastic, 'Война И Мир', 'Толстой', '1865') is False


def test_add_books():
    assert add_books('books', elastic) == 2
    assert add_books('books', elastic) == 0


def test_seacher():
    body = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {"text": 'известием'}
                    }
                ]
            }
        }
    }

    res = searcher(elastic, body)
    assert len(res['hits']['hits']) == 2

def test_exists():
    assert exists(elastic, 'Война И Мир', 'Толстой', '1865') is True
    assert exists(elastic, 'Война И Мир', 'Пушкин', '1865') is False


def test_count_books_with_words():
    assert count_books_with_words(elastic, 'известием') == 2
    assert count_books_with_words(elastic, 'самолет') == 0


def test_search_books():
    assert search_books(elastic, 'Пушкин', 'вином') == 2
    assert search_books(elastic, 'Пушкин', 'самолет') == 0


def test_search_date():
    assert search_date(elastic, '1831', '1836', 'самолет') == 2
    assert search_date(elastic, '1831', '1836', 'известием') == 1


def test_calc_date():
    assert calc_date(elastic, 'Пушкин') == 1834
    assert calc_date(elastic, 'Пупкин') == 0


def test_search_by_year():
    assert len(search_by_year(elastic, '1836')) == 1
    assert len(search_by_year(elastic, '1000')) == 0


def test_top_words():
    assert top_words(elastic, '1836') == ['это', 'отвечал', 'пугачев', 'марья', 'ивановна', 'тебе',
                                          'батюшка', 'савельич', 'швабрин', 'иван']